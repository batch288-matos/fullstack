// If you will create a ReactJS application
	// npx create-react-app project-name
// ReactJS is strict with the application name, it should be in lower case
	// courseBooking - error or we were not able to create an application/project
	// files to be deleted
		// App.test.js
		// index.css
		// logo.svg
		// reportWebVitals.js

// after we delete the files

// courses.js will serve as our mock database in place of our mongoDB database because we want to focus on ReactJS

// Environment variables are important for hiding sensitive pieces of information like the backend API URL which can be exploited if added into our code