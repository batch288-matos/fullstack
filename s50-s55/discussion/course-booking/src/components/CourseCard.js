import {Container, Row, Col, Button, Card} from 'react-bootstrap';
// import the useState hook from react
import {useState, useEffect, useContext} from 'react';

import {Link} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function CourseCard(props){
	const {user} = useContext(UserContext);

	// console.log(props.courseProp);
	// object destructuring
	const {_id, name, description, price} = props.courseProp;

	// Use the state hook for this component to be able to store its state
	// States are use to keep track of information related to individual components
		// Syntax : const [getter, setter] = useState(initialGettervalue);

	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	const [isDisabled, setIsDisabled] = useState(false);
	// console.log(count);

	// setCount(2);
	// console.log(count);


 	// This function will be invoked when the button enroll is clicked 
function enroll(){
	if(count === 30){
		// setIsDisabled(true);
		alert('No more seats!');

	}
	else{
		setCount(count + 1);
		setSeat(seat - 1);
		// alert('Thank you for enrolling!');
		// console.log(count);
	}
}

// The function or the side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is/are change/changes on our depencies
// Syntax: 
	// useEffec(side effect, [dependencies]);
useEffect(() => {
	// console.log('Hi! I am from the useEffect');
	if(seat === 0){
		// it will change the state or value of isDisabled to true
		setIsDisabled(true);
	}
}, [seat]);

	return(
		<Container className = 'mt-3'>
			<Row>
				<Col>
					<Card>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				        	{description}
				        </Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>
				        	{price}
				        </Card.Text>
				        <Card.Subtitle>Enrollees:</Card.Subtitle>
				        <Card.Text>
				        	{count}
				        </Card.Text>
				      </Card.Body>
				      {
						user.id !== null
						?
						<Button as = {Link} to = {`/courses/${_id}`} className = 'col-3 col-xs-2 col-sm-2 col-md-2 col-lg-1 ms-2 mb-2'>Details</Button>
						:
						<Button as = {Link} to = '/login' className = 'col-3 col-xs-2 col-sm-2 col-md-2 col-lg-1 ms-2 mb-2'>Login to Enroll</Button>
					  }
				    </Card>
				</Col>
			</Row>
		</Container>
		);
}