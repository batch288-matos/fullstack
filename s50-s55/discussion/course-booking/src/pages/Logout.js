import {Navigate} from 'react-router-dom';
import {useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

export default function Logout(){
    // To clear the content of our local storage we have to use the clear();
    // localStorage.clear()
    const {unsetUser, setUser} = useContext(UserContext);

    useEffect(() => {
        unsetUser();
        setUser({
            id: null,
            isAdmin: null
        });
    })

    return(
        <Navigate to = '/login'/>
    );
}