import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
import CourseView from './pages/CourseView.js';

// The BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
// The Routes component holds all our Route components. It selects which 'Route' components to show based on the URL endpoint
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';

function App() {

  let userDetails = {};
  useEffect(() => {
    if(localStorage.getItem('token') === null){
      userDetails = {
        id: null,
        isAdmin: null
      }
    }
    else{
      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        method: "GET",
        headers: {
          "Authorization" : `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(result => result.json())
      .then(data => {
        userDetails = {
          id: data.id,
          isAdmin: data.isAdmin
        }
      })
    }
  }, [])

  const [user, setUser] = useState(userDetails);
  const unsetUser = () => {
    localStorage.clear();
  };

  // useEffect(() => {
  //   console.log(user)
  //   console.log(localStorage.getItem('token'))
  // }, [user])

  // useEffect(() => {
  //   if(localStorage.getItem('token')) {
  //     fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
  //       method: 'GET',
  //       headers: {
  //         Authorization: `Bearer ${localStorage.getItem('token')}`
  //       }
  //     })
  //     .then(result => result.json())
  //     .then(data => {
  //       // console.log(data);
  //       setUser({
  //         id: data._id,
  //         isAdmin: data.isAdmin
  //       })
  //     })
  //   }
  // }, [])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <Routes>
          <Route path = '/' element = {
            <>
            <AppNavBar/>
            <Home/>
            </>
          }/>
          <Route path = '/courses' element = {
            <>
            <AppNavBar/>
            <Courses/>
            </>
          }/>
          {
            user.id === null
            ?
            <>
            <Route path = '/register' element = {
              <>
              <AppNavBar/>
              <Register/>
              </>
            }/>
            <Route path = '/login' element = {
              <>
              <AppNavBar/>
              <Login/>
              </>
            }/>
            </>
            :
            <>
            <Route path = '/register' element = {<PageNotFound/>}/>
            <Route path = '/login' element = {<PageNotFound/>}/>
            </>
          }
          <Route path = '/logout' element = {
            <>
            <AppNavBar/>
            <Logout/>
            </>
          }/>
          <Route path = '/courses/:courseId' element = {
              <>
              <AppNavBar/>
              <CourseView/>
              </>
            }/>
          <Route path ='*' element = {<PageNotFound/>}/>
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
